<!-- Begin Banner -->
	<section class="banner wow fadeIn" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'quienes-somos' ) ) ) : dynamic_sidebar( 'banner_quienes_somos' ); endif; ?>
				<?php if ( is_page( array( 'redes-electricas' ) ) ) : dynamic_sidebar( 'banner_redes_electricas' ); endif; ?>
				<?php if ( is_page( array( 'cableado-estructurado' ) ) ) : dynamic_sidebar( 'banner_cableado_estructurado' ); endif; ?>
				<?php if ( is_page( array( 'sistemas-de-seguridad' ) ) ) : dynamic_sidebar( 'banner_sistemas_de_seguridad' ); endif; ?>
				<?php if ( is_page( array( 'obras-civiles' ) ) ) : dynamic_sidebar( 'banner_obras_civiles' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->