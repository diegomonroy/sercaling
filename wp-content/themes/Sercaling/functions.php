<?php

/*

Functions for my template

*/

/*
 * Function to add my styles files
 */
function my_styles_files() {
	wp_enqueue_style( 'foundation-flex-css', get_template_directory_uri() . '/build/foundation/css/foundation.min.css', false );
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/build/bower_components/animate.css/animate.min.css', false );
	wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/build/bower_components/fancybox/dist/jquery.fancybox.min.css', false );
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/build/bower_components/font-awesome/css/font-awesome.min.css', false );
	if ( is_child_theme() ) {
		wp_enqueue_style( 'parent-css', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
	}
	wp_enqueue_style( 'theme-css', get_stylesheet_uri(), false );
}
add_action( 'wp_enqueue_scripts', 'my_styles_files' );

/*
 * Function to add my scripts files
 */
function my_scripts_files() {
	//wp_deregister_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_files' );

/*
 * Function to add my scripts files in footer
 */
function my_scripts_files_footer() {
	//wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/build/bower_components/jquery/dist/jquery.min.js', false );
	wp_enqueue_script( 'what-input-js', get_template_directory_uri() . '/build/bower_components/what-input/dist/what-input.min.js', false );
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/build/foundation/js/vendor/foundation.min.js', false );
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/build/bower_components/fancybox/dist/jquery.fancybox.min.js', false );
	wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/build/bower_components/wow/dist/wow.min.js', false );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/build/app.js', false );
}
add_action( 'wp_footer', 'my_scripts_files_footer' );

/*
 * Function to register my menus
 */
function register_my_menus() {
	register_nav_menus(
		array(
			'main-menu' => __( 'Main Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

/*
 * Function to add my custom menu item
 */
function my_custom_menu_item( $items, $args ) {
	if ( $args->theme_location == 'main-menu' ) {
		$items .= '<li><a href="https://www.facebook.com" target="_blank"><i class="fa fa-facebook"></i></a></li>'; // Facebook
		$items .= '<li><a href="https://www.instagram.com" target="_blank"><i class="fa fa-instagram"></i></a></li>'; // Instagram
	}
	return $items;
}
add_filter( 'wp_nav_menu_items', 'my_custom_menu_item', 10, 2 );

/*
 * Function to register my sidebars and widgetized areas
 */
function arphabet_widgets_init() {
	register_sidebar(
		array(
			'name' => 'Logo',
			'id' => 'logo',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Buscar',
			'id' => 'search',
			'before_widget' => '<div class="moduletable_to3">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Inicio',
			'id' => 'banner_inicio',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Quiénes Somos',
			'id' => 'banner_quienes_somos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Redes Eléctricas',
			'id' => 'banner_redes_electricas',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Cableado Estructurado',
			'id' => 'banner_cableado_estructurado',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Sistemas de Seguridad',
			'id' => 'banner_sistemas_de_seguridad',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Obras Civiles',
			'id' => 'banner_obras_civiles',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 1',
			'id' => 'block_1',
			'before_widget' => '<div class="moduletable_b11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 2',
			'id' => 'block_2',
			'before_widget' => '<div class="moduletable_b21">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 3',
			'id' => 'block_3',
			'before_widget' => '<div class="moduletable_b31">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="text-center">',
			'after_title' => '</h3>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 4',
			'id' => 'block_4',
			'before_widget' => '<div class="moduletable_b41">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Bottom',
			'id' => 'bottom',
			'before_widget' => '<div class="moduletable_bo1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
}
add_action( 'widgets_init', 'arphabet_widgets_init' );

/*
 * Custom shortcode to Service
 */
function shortcode_service() {
	$web = get_site_url() . '/';
	if ( is_page( 'redes-electricas' ) ) : $redes_electricas = '_active'; endif;
	if ( is_page( 'cableado-estructurado' ) ) : $cableado_estructurado = '_active'; endif;
	if ( is_page( 'sistemas-de-seguridad' ) ) : $sistemas_de_seguridad = '_active'; endif;
	if ( is_page( 'obras-civiles' ) ) : $obras_civiles = '_active'; endif;
	$html = '
		<div class="row" data-equalizer data-equalize-on="medium" id="home-1-eq">
			<div class="small-12 medium-3 columns">
				<p class="text-center"><a href="' . $web . 'nuestros-servicios/redes-electricas"><img src="' . $web . 'wp-content/uploads/Block-1-Redes-Eléctricas' . $redes_electricas . '.png" title="REDES ELECTRICAS" alt="REDES ELECTRICAS"></a></p>
				<p class="text-center no-margin" data-equalizer-watch><a href="' . $web . 'nuestros-servicios/redes-electricas">REDES ELECTRICAS</a></p>
			</div>
			<div class="small-12 medium-3 columns">
				<p class="text-center"><a href="' . $web . 'nuestros-servicios/cableado-estructurado"><img src="' . $web . 'wp-content/uploads/Block-1-Cableado-Estructurado' . $cableado_estructurado . '.png" title="CABLEADO ESTRUCTURADO" alt="CABLEADO ESTRUCTURADO"></a></p>
				<p class="text-center no-margin" data-equalizer-watch><a href="' . $web . 'nuestros-servicios/cableado-estructurado">CABLEADO ESTRUCTURADO</a></p>
			</div>
			<div class="small-12 medium-3 columns">
				<p class="text-center"><a href="' . $web . 'nuestros-servicios/sistemas-de-seguridad"><img src="' . $web . 'wp-content/uploads/Block-1-Sistemas-de-Seguridad' . $sistemas_de_seguridad . '.png" title="SISTEMAS DE SEGURIDAD" alt="SISTEMAS DE SEGURIDAD"></a></p>
				<p class="text-center no-margin" data-equalizer-watch><a href="' . $web . 'nuestros-servicios/sistemas-de-seguridad">SISTEMAS DE SEGURIDAD</a></p>
			</div>
			<div class="small-12 medium-3 columns">
				<p class="text-center"><a href="' . $web . 'nuestros-servicios/obras-civiles"><img src="' . $web . 'wp-content/uploads/Block-1-Obras-Civiles' . $obras_civiles . '.png" title="OBRAS CIVILES" alt="OBRAS CIVILES"></a></p>
				<p class="text-center no-margin" data-equalizer-watch><a href="' . $web . 'nuestros-servicios/obras-civiles">OBRAS CIVILES</a></p>
			</div>
		</div>
	';
	return $html;
}
add_shortcode( 'service', 'shortcode_service' );