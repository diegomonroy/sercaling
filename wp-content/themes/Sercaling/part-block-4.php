<!-- Begin Block 4 -->
	<section class="block_4 wow fadeIn" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'block_4' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 4 -->